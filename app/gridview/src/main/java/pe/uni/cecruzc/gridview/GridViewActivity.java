package pe.uni.cecruzc.gridview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class GridViewActivity extends AppCompatActivity {

    GridView gridView;
    ArrayList<String> text = new ArrayList<>();
    ArrayList<Integer> image = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view);

        gridView = findViewById(R.id.grid_view);
        fillArray();

        GridAdapter gridAdapter = new GridAdapter(this, text, image);
        gridView.setAdapter(gridAdapter);


        gridView.setOnItemClickListener((parent, view, position, id) -> Toast.makeText(getApplicationContext(), text.get(position), Toast.LENGTH_LONG).show());
    }

    private void fillArray() {
        text.add("Fox");
        text.add("Hipo");
        text.add("Lion");
        text.add("Monkey");
        text.add("Pig");

        image.add(R.drawable.fox);
        image.add(R.drawable.hipo);
        image.add(R.drawable.lion);
        image.add(R.drawable.monkey);
        image.add(R.drawable.pig);
    }
}