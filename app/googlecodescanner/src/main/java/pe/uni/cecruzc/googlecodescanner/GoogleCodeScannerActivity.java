package pe.uni.cecruzc.googlecodescanner;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bosphere.filelogger.FL;
import com.google.mlkit.common.MlKitException;
import com.google.mlkit.vision.barcode.common.Barcode;
import com.google.mlkit.vision.codescanner.GmsBarcodeScanner;
import com.google.mlkit.vision.codescanner.GmsBarcodeScannerOptions;
import com.google.mlkit.vision.codescanner.GmsBarcodeScanning;

public class GoogleCodeScannerActivity extends AppCompatActivity {

    TextView textViewScanner;
    Button buttonScanner;

    private static final String TAG = GoogleCodeScannerActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        FL.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_code_scanner);

        textViewScanner = findViewById(R.id.text_view_scanner);
        buttonScanner = findViewById(R.id.button_scanner);

        buttonScanner.setOnClickListener(v -> {
            GmsBarcodeScannerOptions option = new GmsBarcodeScannerOptions.Builder()
                    .setBarcodeFormats(Barcode.FORMAT_QR_CODE)
                    .build();

            GmsBarcodeScanner scanner = GmsBarcodeScanning.getClient(getApplicationContext(), option);

            scanner
                    .startScan()
                    .addOnSuccessListener(barcode -> {
                        FL.i("addOnSuccessListener");
                        textViewScanner.setText(getSuccessfulMessage(barcode));
                    })
                    .addOnCanceledListener(() -> {
                        FL.i("addOnCanceledListener");
                        textViewScanner.setText(R.string.canceled_option);
                    })
                    .addOnFailureListener(e -> {
                        FL.i(TAG, "Error: %s", e);
                        textViewScanner.setText(getFailureExceptionMessage((MlKitException) e));
                    });
        });
    }

    private String getSuccessfulMessage(Barcode barcode) {
        FL.i(TAG, "getSuccessfulMessage");
        return String.format(
                getResources().getString(R.string.barcode_result),
                barcode.getRawValue(),
                barcode.getFormat(),
                barcode.getValueType());
    }

    private String getFailureExceptionMessage(MlKitException e) {
        FL.i(TAG, "getFailureExceptionMessage");
        switch (e.getErrorCode()) {
            case MlKitException.CODE_SCANNER_CANCELLED:
                return getString(R.string.error_code_scanner_canceled);
            case MlKitException.UNKNOWN:
                return getString(R.string.error_code_unknown);
            default:
                return getString(R.string.error_default);
        }
    }
}